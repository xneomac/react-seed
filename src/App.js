import React from 'react';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';

import { store, history } from './store';
import { Helmet } from './helmet';
import { Main } from './containers/Main';

export class App extends React.Component
{
  render()
  {
    return (
      <div>
        <Helmet/>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <Switch>
              <Route path="/" component={Main}/>
            </Switch>
          </ConnectedRouter>
        </Provider>
      </div>
    )
  }
}
