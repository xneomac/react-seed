import React from 'react';

/**
 *
 */
export class Main extends React.Component
{
  constructor(props)
  {
    super(props);
  }

  render()
  {
    return (
      <section className="hero">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              {'React Seed'}
            </h1>
            <h2 className="subtitle">
              {'A simple react seed to meet all your needs.'}
            </h2>
          </div>
        </div>
      </section>
    )
  }
}
