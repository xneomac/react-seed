import { connect } from 'react-redux';
import { Main as Component } from '../components/Main';
import { push } from 'react-router-redux';

let mapStateToProps = (state) =>
{
  return {};
}

let mapDispatchToProps = (dispatch) =>
{
  return {
    redirect: url => dispatch(push(url)),
  }
}

export let Main = connect(mapStateToProps, mapDispatchToProps)(Component);
