import React from 'react';
import { Helmet as ReactHelmet } from 'react-helmet';

export const Helmet = () => (
  <ReactHelmet>
    <meta charSet="utf-8" />
    <title>{'React Seed'}</title>
    <link rel="manifest" href="manifest.json" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css" />
    <link rel="icon" href="favicon.ico" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
  </ReactHelmet>
)
